BI-FIT_CVUT subject focused on computer structures. In the first half of the semester we worked with FPGA Basys 3 
either writing VHDL or drawing the schematic (logic gates) using Meally or Moore final-state machine. 
We had many tasks like multiplier, adder , 7-segment decoder and the final task Code lock.


In the second part of the semester we learned basics of Asembler. We used ATMEGA168 AVR microcontroller from Atmel.
In tasks we learned the basic commands, loops, interupts etc. For example there were tasks like : Arithmetic operations,
Showing 8 bit number in hex on display, rotating text and the final timer project.
